#include <sys/types.h>                                                                                                      // Required by getaddrinfo(), setsockopt(), bind(), listen(), accept(), recv()
#include <sys/socket.h>                                                                                                     // Required by getaddrinfo(), setsockopt(), bind(), listen(), accept(), recv()
#include <netdb.h>                                                                                                          // Required by getaddrinfo()
#include <string.h>                                                                                                         // Required by memset()
#include <stdio.h>                                                                                                          // Required by perror()
#include <stdlib.h>                                                                                                         // Required by exit()
#include <unistd.h>

#include <pthread.h>

#include "sockList.h"

socketList sockets;
static pthread_mutex_t lockSockets = PTHREAD_MUTEX_INITIALIZER;

void postman(char* buffer){
	sList it;
	size_t messageSize = strlen(buffer);
	pthread_mutex_lock(&lockSockets);
	for(it=*(sockets->start); it != NULL; it = it->next){
		send(it->socket,buffer,messageSize,0);
	}
	pthread_mutex_unlock(&lockSockets);
}

void* workerFn(void* arg){
	sList* client = arg;
	int clientSocket = (*client)->socket;
	size_t bufferSize = 1000;
	char* message;
	char buffer[bufferSize];
	size_t received;	
	memset(&buffer,0,sizeof(char)*bufferSize);
	send(clientSocket,"Username?\n",strlen("Username\n"),0);
	received = recv(clientSocket,buffer,bufferSize,0);
	if(received == -1){
		perror("Failed to receive message!");
		exit(EXIT_FAILURE);
	}
	buffer[received-1]=':';
	message = buffer+received;
	
	while(strcmp(message,"quit\n")!=0){
		received = recv(clientSocket, message, bufferSize,0);
		if(received == -1){
			perror("Failed to receive message!");
			exit(EXIT_FAILURE);
		}
		printf("Received message\n");
		message[received] = '\0';
		postman(buffer);
	}
	close(clientSocket);
	pthread_mutex_lock(&lockSockets)
	rmSocket(client);
	pthread_mutex_unlock(&lockSockets)
	printf("Client quit!\n");
}

int main(int argc, char **argv){
	sockets = initSL();
	char *portNumber = "6969";
	int backlog = 1;
	int serverSocket;
	pthread_t worker;
	struct addrinfo hints;
	memset(&hints, 0, sizeof(struct addrinfo));
	struct addrinfo *results;
	struct addrinfo *record;
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	if((getaddrinfo(NULL,portNumber,&hints,&results)) != 0){
		perror("Failed to translate server socket.");
		exit(EXIT_FAILURE);
	}
	printf("Server socket translated.\n");

	for(record = results; record != NULL; record = record->ai_next){
		printf("%p\n",record);
		serverSocket = socket(record->ai_family,record->ai_socktype,record->ai_protocol);
		if(serverSocket != -1){
			int enable = 1;
			setsockopt(serverSocket,SOL_SOCKET,SO_REUSEADDR, &enable, sizeof(int));
		}
		if (bind(serverSocket, record->ai_addr, record->ai_addrlen) == 0){ 
			break;
		}
		close(serverSocket);
	}
	
	if(record == NULL){
		perror("Failed to create or connect server socket.");
		exit(EXIT_FAILURE);
	}
	
	freeaddrinfo(results);

	printf("Server socket creared and bound.\n");

	if(listen(serverSocket,backlog) == -1){
		perror("Failed to start server socket listen.");
		exit(EXIT_FAILURE);
	}

	printf("Server started listening.\n");

	while(1){
		int clientSocket;
		sList* client;

		struct sockaddr clientAddress;
		socklen_t clientAddressLength = sizeof(clientAddress);

		if((clientSocket = accept(serverSocket, &clientAddress, &clientAddressLength)) < 0){
			perror("Failed to accept client socket.");
			exit(EXIT_FAILURE);
		}

		printf("Client socket accepted!\n");
		pthread_mutex_lock(&lockSockets);
		client = addSocket(sockets,clientSocket);
		pthread_mutex_unlock(&lockSockets);
		pthread_create(&worker,NULL,workerFn,client);
	}

}
