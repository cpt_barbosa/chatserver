#include <stdlib.h>
#include <stdio.h>
#include "sockList.h"
socketList initSL(){
	socketList r;
	r = malloc(sizeof(struct socketList));
	r->start=NULL;
	r->last=NULL;
}

void freeSL(socketList sockets){
	sList it;
	sList t;
	it = *(sockets->start);
	while(it != NULL){
		t = it;
		it = it->next;
		free(t);
	}
	free(sockets);
}

sList* addSocket(socketList sockets, int clientSocket){
	sList* tmp;
	sList* r;
	tmp = malloc(sizeof(sList));
	(*tmp) = malloc(sizeof(struct socketlist));
	(*tmp)->socket = clientSocket;
	(*tmp)->next = NULL;

	if(sockets->last!=NULL){
		r = (sockets->last);
		*(sockets->last) = *tmp;
	}
	else{
		sockets->start = tmp;
		r = sockets->start;
	}

	sockets->last = &((*tmp)->next);
	return r;
}

void rmSocket(sList* clientNode){
	sList t;
	t = *clientNode;
	*clientNode = (*clientNode)->next;	
	free(t);
}

void printSL(socketList sockets){
	sList it;
	it = *(sockets->start);
	while(it != NULL){
		printf("%d->",it->socket);
		it = it->next;
	}
	printf("x\n");
}

