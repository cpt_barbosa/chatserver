#include <stdlib.h>

typedef struct socketlist{ 
	int socket;
	struct socketlist* next;
}*sList;

typedef struct socketList{
	sList* start;
	sList* last;
}*socketList;

socketList initSL();

void freeSL(socketList);

sList* addSocket(socketList, int);

void rmSocket(sList*);

void printSL(socketList);
